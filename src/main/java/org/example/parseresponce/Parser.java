package org.example.parseresponce;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Component
public class Parser {


    public String getParseText(String string, String rex) {

        Pattern pattern = Pattern.compile(rex);
        Matcher matcher = pattern.matcher(string);


        String lastnews = null;
        if (matcher.find()) {
            lastnews = matcher.group(2);
        }


        return lastnews.substring(1, lastnews.length() - 1);
    }


}
