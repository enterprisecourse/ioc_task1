package org.example.source;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import org.example.dto.ResponceBingNews;
import org.example.dto.ResponceNewsSearch;
import org.example.parseresponce.Parser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;


@Component
public class SourceBingNewsSearch implements Source{
    ObjectMapper objectMapper = new ObjectMapper();



    @Getter
    @Setter
    @Autowired
    Parser parser;

    @Override
    public String getNews() throws IOException, InterruptedException {


        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://news-search4.p.rapidapi.com/news"))
                .header("content-type", "application/x-www-form-urlencoded")
                .header("x-rapidapi-key", "f0a89e287bmsh1161f4fa4fcb731p14ad39jsn27f4a668ac8c")
                .header("x-rapidapi-host", "news-search4.p.rapidapi.com")
                .method("POST", HttpRequest.BodyPublishers.ofString("find=America%20Market&sortby=popular"))
                .build();

        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        String status = response.body();

        ResponceNewsSearch news = objectMapper.readValue(status, ResponceNewsSearch.class);
        String string = String.valueOf(news.getResponse()[0]);

        String lastnes = parser.getParseText(string,"(content((=.+:)))");
        return lastnes;
    }
}
