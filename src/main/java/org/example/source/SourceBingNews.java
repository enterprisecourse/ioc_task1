package org.example.source;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import org.example.dto.ResponceBingNews;
import org.example.parseresponce.Parser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Component
public class SourceBingNews implements Source{
    ObjectMapper objectMapper = new ObjectMapper();

    @Getter
    @Setter
    @Autowired
    Parser parser;

    @Override
    public String getNews() throws IOException, InterruptedException {



        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://bing-news-search1.p.rapidapi.com/news?textFormat=Raw&safeSearch=Off"))
                .header("x-bingapis-sdk", "true")
                .header("x-rapidapi-key", "f0a89e287bmsh1161f4fa4fcb731p14ad39jsn27f4a668ac8c")
                .header("x-rapidapi-host", "bing-news-search1.p.rapidapi.com")
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();


        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        String status = response.body();

        ResponceBingNews news = objectMapper.readValue(status, ResponceBingNews.class);
        Object newsA []= news.getValue();

        String string = String.valueOf(newsA[newsA.length-1]);

        String newsString = parser.getParseText(string,"(description(.+) ()(provider))");

        return newsString;
    }
}
