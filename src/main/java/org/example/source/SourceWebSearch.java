package org.example.source;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import org.example.dto.ResponceWebSearch;
import org.example.parseresponce.Parser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;


@Component
public class SourceWebSearch implements Source{

    ObjectMapper objectMapper = new ObjectMapper();

    @Getter
    @Setter
    @Autowired
    Parser parser;

    @Override
    public String getNews() throws IOException, InterruptedException {


        ObjectMapper objectMapper = new ObjectMapper();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://contextualwebsearch-websearch-v1.p.rapidapi.com/api/search/TrendingNewsAPI?pageNumber=1&pageSize=10&withThumbnails=false&location=us"))
                .header("x-rapidapi-key", "f0a89e287bmsh1161f4fa4fcb731p14ad39jsn27f4a668ac8c")
                .header("x-rapidapi-host", "contextualwebsearch-websearch-v1.p.rapidapi.com")
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();

        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        String status = response.body();

        ResponceWebSearch news = objectMapper.readValue(status, ResponceWebSearch.class);

        String string = String.valueOf(news.getValue()[0]);

        String str1 = string.substring(0,string.length()-1);

        String newsString = parser.getParseText(string,"(description(.+)(body))");



        return newsString.substring(0,newsString.length()-1);
    }



}
