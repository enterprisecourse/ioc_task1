package org.example.source;

import org.example.parseresponce.Parser;

import java.io.IOException;

public interface Source {


    public String getNews() throws IOException, InterruptedException;


}
