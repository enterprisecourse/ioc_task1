package org.example.dto;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ResponceWebSearch {

    String _type;
    String didUMean;
    String totalCount;
    Object relatedSearch[];
    Object value[];



}
