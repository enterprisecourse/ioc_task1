package org.example.dto;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ResponceNewsSearch {

    String status;
    String code;
    String msg;
    Object response[];
    Object info;


}
